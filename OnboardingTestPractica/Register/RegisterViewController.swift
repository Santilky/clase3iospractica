//
//  RegisterViewController.swift
//  OnboardingTestPractica
//
//  Created by Santiago Linietsky on 03/02/2022.
//

import UIKit

class RegisterViewController: UIViewController {

    lazy var label: UILabel = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // Do any additional setup after loading the view.
    }
    func setupUI() {
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Register!"
        NSLayoutConstraint.activate(
            [
                label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ]
        )
    }
}
