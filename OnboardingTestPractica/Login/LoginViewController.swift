//
//  LoginViewController.swift
//  OnboardingTestPractica
//
//  Created by Santiago Linietsky on 03/02/2022.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginButton.addTarget(self, action: #selector(self.logear), for: .touchDown)
    }
    @objc func logear() {
        self.modalPresentationStyle = .overFullScreen
        present(ModalTest(), animated: true)
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
