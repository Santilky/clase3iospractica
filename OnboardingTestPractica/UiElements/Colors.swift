//
//  Colors.swift
//  OnboardingTestPractica
//
//  Created by Santiago Linietsky on 03/02/2022.
//

import Foundation
import UIKit

enum Colors {
    case transparentBlack
    case red
    
    func getColor() -> UIColor {
        switch self {
        case .transparentBlack:
            return .init(red: 25/255, green: 25/255, blue: 25/255, alpha: 0.3)
        case .red:
            return .init(red: 244/255, green: 50/255, blue: 25/255, alpha: 1)
        }
    }
}
