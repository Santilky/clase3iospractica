//
//  ModalTest.swift
//  OnboardingTestPractica
//
//  Created by Santiago Linietsky on 03/02/2022.
//

import Foundation
import UIKit

class ModalTest: UIViewController {
    private lazy var solidBackground = UIView()
    private lazy var button = UIButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.view.addSubview(solidBackground)
        solidBackground.translatesAutoresizingMaskIntoConstraints = false
        self.view.backgroundColor = Colors.transparentBlack.getColor()
        self.solidBackground.backgroundColor = Colors.red.getColor()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Volver!", for: .normal)
        button.addTarget(self, action: #selector(self.close), for: .touchDown)
        solidBackground.addSubview(button)
        solidBackground.layer.cornerRadius = 15
        NSLayoutConstraint.activate(
            [
                solidBackground.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
                solidBackground.leftAnchor.constraint(equalTo: self.view.leftAnchor),
                solidBackground.rightAnchor.constraint(equalTo: self.view.rightAnchor),
                solidBackground.heightAnchor.constraint(equalToConstant: 200),
                button.centerXAnchor.constraint(equalTo: solidBackground.centerXAnchor),
                button.centerYAnchor.constraint(equalTo: solidBackground.centerYAnchor)
            ]
        )
    }
    @objc func close() {
        self.dismiss(animated: true)
    }
}
