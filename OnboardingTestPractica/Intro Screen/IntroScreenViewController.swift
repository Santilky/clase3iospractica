//
//  IntroScreenViewController.swift
//  OnboardingTestPractica
//
//  Created by Santiago Linietsky on 03/02/2022.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol IntroScreenDisplayLogic: class
{
  
}

class IntroScreenViewController: UIViewController, IntroScreenDisplayLogic
{
  var interactor: IntroScreenBusinessLogic?
  var router: (NSObjectProtocol & IntroScreenRoutingLogic & IntroScreenDataPassing)?

  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = IntroScreenInteractor()
    let presenter = IntroScreenPresenter()
    let router = IntroScreenRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
      super.viewDidLoad()
  }
  
  // MARK: Do something
  
    @IBAction func logInAction(_ sender: UIButton) {
        let vc = LoginViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func registerAction(_ sender: UIButton) {
        let vc = RegisterViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //@IBOutlet weak var nameTextField: UITextField!
  
}
